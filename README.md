# README #

Tornson is a bloody simple Tornado framework, getting its content from static JSON files. This is actually the code running on http://menu.bartstroeken.nl :)

### What is this repository for? ###

* I wanted to have a go with Tornado
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* I wanted to have a go with the Mako-template language. Spoiler: I'd stick to Jinja2 if I were you. 

### How do I get set up? ###

* Clone the code, create your virtualenv, activate it and 
```
pip install -r requiremements.txt
```

* Configuration: you'll find it. All is in settings.py. 
* How to run tests: start your browser and see if you have a visual. Did not bother to write unit tests for this. Sorry!
* Deployment instructions: Just clone the code and run it. Put a webserver in front of it if you'd like. Up to you!