Appetizers
==========
Creative
Out of the box
Curious
Outgoing
Enthusiastic
Dedicated
Honest
Teamplayer
Musician

Starters
========

1996 - 2005 Wageningen University - MSc Bioprocess Engineering
        Let's have a look at wine. Wine is made of grapes, that are squashed and fermented. For that fermentation, you need yeast. Yeast is a living thing, it eats, grows and multiplies. And it rewards you with alcohol as a Thank You For Feeding Me. 
        But when you want to make lots of wine, you need to use lots of yeast. And you'll have to feed them, all equally. That's a complete different science than feeding and breeding them in the first place.
        That's what this education is about. Both the microbiological and the process engineering side. And of course, not only wine. Think biofuels. Think Functional Foods. Think pharmaceuticals, for example the substances that are retrieved from sponges. 

        Thesises: 
        Design of digital educational material: extraction

        Control of spatial distributed qualities in model food

        Internship:
        Creating hydrogen gas by vaporising biomass in supercritical water

Mains
=====

2013 - nu : Software Developer Nu.nl/News

        Working on quite a complex website ecosystem, built on the concept of microservices: do one thing, do it really well, and make sure that others can use whatever you're producing.
        This was a time of many big projects: pulling up new extension sites like Nutech.nl, nusport.nl and wtf.nl, and ending up with integrating them into the main site. But, most importantly: starting out fresh with a brand new API based responsive frontend!
        Also, a vast period of transitions, content integration and content migrations.

        lessons learned
        You built it, you run it. Make sure that you can backtrace your steps. 
        
        tools used
        "Python/Django",
        "PHP/ATK",
        "MySQL",
        "New Relic",
        "Sentry",
        "Jenkins",
        "ElasticSearch/LogStash"

2011 - 2013 : (Lead) Software Developer Online Women
        
        "<p>A time where the only constant was continuous change. The first months were marked with departures. Our lead developer left, and I took over his job.</p>"
        "<p>Our manager left. An interim manager came, being replaced a few months later. Practical procedures had to be defined in how we work together with our business and within our team - in which I played an important part.</p>"

        Libelle.nl, Margriet.nl, Viva.nl

        SWOP
        Stepaday
        REACT

Desert
======

Shoot for the moon, you'll end up with the stars
Don't repeat yourself
Code For Colleagues: good code can be read by colleagues, and run by computers 
Problem solver
Analysing
Researcher
I Don't Fear Responsibilities
Never Assume, it makes an Ass of U and Me
When I support a goal, I go for it. 



