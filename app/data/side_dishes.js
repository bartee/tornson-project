{
	"title": "Sidedishes",
	"introduction" : "Side projects to complement the main courses. Independent of my daytime employers. I think these projects are a very valuable way to spend my free time.",
    "items": [
        {
			"title": "GetCoding",
			"subtitle": "Owner/Developer",
			"year": "2011 - current",
			"image": "/static/images/ws_getcoding.jpg",
			"content": [
				"<p><a href='http://www.getcoding.nl/' target='_blank'>GetCoding</a> is how I see the future of ",
                "prototyping - or even building (web-) apps. You specify what you want, and push a button to get ",
                "a working prototype. Running code. So you can customize it, or have it customized further, if ",
                "you're not a developer yourself. </p>",
                "<p>Personally, I think that frameworks often obfuscate too much of your common logic by trying to ",
                "simplify common tasks with convenience functions. These frameworks need maintenance, new features, ",
                "ways to work around them - since you'll have to cut corners in order to keep it simple. </p>",
                "<p>But I believe there is a way that you can speed up development time ánd get flexible results. ",
                "I think that way is generating code, and finding a way to build on that generated code.</p>"
			]
		},
        {
			"title": "Ga je mee uit",
			"subtitle": "Owner/Developer",
			"year": "2007 - current",
			"image": "/static/images/ws_gajemeeuit_tp.jpg",
			"content": [
				"<p>As an amateur performer I've seen quite some performing companies. Being a developer, often I ",
                " was asked to take care of the online ticket sales. I didn't want to repeat myself, and back then ",
                " there weren't too many online solutions I could find. So I've built ",
                "<a href='http://www.gajemeeuit.nl/' target='_blank'>Ga je mee uit</a></p>",
                "<p>It's still being used by some companies, but I have to admit that there is actually not ",
                "much active development going on in this project.</p>"
			]
		},
        {
			"title": "De Hollandse Meesters",
			"subtitle": "Singer/Guitarist/Developer",
			"year": "2011 - current",
			"image": "/static/images/ws_hollandsemeesters.jpg",
			"content": [
				"<p>Since this is my resume, let me start by saying I've set up the little Wordpress site of ",
                "<a href='http://www.dehollandsemeesters.com' target='_blank'>De Hollandse Meesters</a>. </p>",
                "<p>And yes, you'll see me playing and singing there too! </p>"
			]
		},
        {
			"title": "Sempre Sereno",
			"subtitle": "Developer/Performer",
			"year": "2001 - 2014",
			"image": "/static/images/ws_sempre.jpg",
			"content": [
				"<p>My favourite amateur musical company. Ambitious, driven, and warm. You cannot find it ",
                "online anymore, but I've built an online archive. Showing each show the company performed. Showing ",
                "the role each player played over the years. Built a forum. And, this was the main reason why I've ",
                "started Ga je mee uit.</p>",
                "<p>And yes, I've played my parts as well. In Moulin Rouge (Christian), in Hair (Woof), in the ",
                " Rocky Horror Show (Riff Raff). Among tons of other things."
			]
		}

    ]
}