{
	"title" : "Chef's Note",
	"introduction" :[ 
		"<p>Welcome! I'm Bart Stroeken, and I'm a software developer. In my career, I get quite a lot of ",
		" interesting things on my plate. That's what this menu is about: what I had on my plate up to here. My resume.</p>",
		"<p>I’m a full stack developer. Frontend, backend, designing and provisioning applications (and) environments - I’ve done it all. I’ve ",
		"set up my share of continuous integration stacks. I know how to integrate these elements, and how to ",
		"test them. I love the Microservices-paradigm.  <br/>",
		"It’s never just one of these elements - it all has to play nice together in order to make an application ",
		"tick.</p>",
		"<p>Although I'm very experienced with quite some languages - like PHP and python, for example - in the end I ",
		" consider myself language agnostic. It's the means to an end. If it is promising or necessary, I'll give it ",
		"a go and learn to understand a new language or framework.</p>",
		"<p>Whatever I'm building, people should want to use it. That's what I want and tend to do. <br/>Add a little ",
		"extra to people's lives.</p>",
		"<p>The menu you see before you represents the current state of my career. As a professional, I'm <b>curious</b>,",
		" <b>dedicated</b> and an <b>honest</b> teamplayer. As a person, I'm <b>creative</b>, <b>enthusiastic</b> and ",
		"I like to think <b>out of the box</b>. <br/> Have a look around! Click around, and scroll around.</p>",
		"<p><small>in case you're interested, <a href=\"https://bitbucket.org/bartee/tornson-project\" target=\"_blank\">the source of this site can be found here!</a></small></p>"],
	"items" : [
		{
			"name": "Starters",
			"icon": "normal",
            "description": "An educated set of starters"
		},
	    {
			"name": "Mains",
			"icon": "normal",
            "description": "The main courses of my career"
		},
        {
			"name": "Side dishes",
			"icon": "normal",
            "description": "Side projects to complement the main courses"
		},
	    {
			"name": "Desserts",
			"icon": "normal",
            "description": "Treats and traits from the chef"
		}
	],
    "items_to_flatten" : [
    	"introduction"
    ]
}