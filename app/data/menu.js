{
	"chefs_note": "About the menu",
	"starters": "An educated set of starters",
	"mains": "The main courses of my career",
	"side_dishes": "Side projects to complement the main courses",
    "desserts": "Treats and traits from the chef"
}