{
	"title": "Desserts",
	"content" : [
            "<h3>I'm a full stack developer.</h3>",
            "<p>Both frontend and backend, I know my way around. I can handle HTML and JavaScript as well as PHP and Python.",
            "From database optimization to caching servers. I've set up clusters of servers and developed quite a list of ",
            "applications.</p>",
            "<a href='https://xhtmlized.com/blog/the-anatomy-of-a-web-developer-infographic/' target='_blank'>",
            "<img src='https://s3-us-west-2.amazonaws.com/xhtmlized-com/wp-content/uploads/2015/01/anatomy-of-web-developer.png' ",
            "class='floated_image'></a>",
            "<p>You've read about some tools and skills in the <a href='/mains'> mains</a>. It's not all I can do",
            "- and it shouldn't be. As a developer, you'll also need the right attitude and a clear way of thinking. ",
            "When I've built this little menu card, I came across this ",
            "<a href='https://xhtmlized.com/blog/the-anatomy-of-a-web-developer-infographic/' target='_blank'>",
            "infographic</a> about the anatomy of a web developer. It actually says it all. </p>"

    ]
}